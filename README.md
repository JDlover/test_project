<h1>Setting up docker to launch projects locally</h1>

1. Установите докер по инструкции:
https://docs.docker.com/engine/install/

Если у вас ubuntu, то можете воспользоваться следующей подробной инструкцией по установке докера:
  - Откройте терминал и обновите apt:
<br>`$ sudo apt-get update`
  - Установите пакеты, чтобы apt использовал репозиторий по https:
####
    $ sudo apt-get install 
    apt-transport-https 
    ca-certificates 
    curl 
    gnupg 
    lsb-release

  - Добавьте GPG-ключ докера (такой ключ специальный для линукса):
<br>`$ curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo gpg --dearmor -o /usr/share/keyrings/docker-archive-keyring.gpg
`
  - Настройте репозиторий, из которого будет устанавливаться докер:
<br>`$ echo \
  "deb [arch=$(dpkg --print-architecture) signed-by=/usr/share/keyrings/docker-archive-keyring.gpg] https://download.docker.com/linux/ubuntu \
  $(lsb_release -cs) stable" | sudo tee /etc/apt/sources.list.d/docker.list > /dev/null`
  - На всякий случай:
<br>`$ sudo apt-get update`
  - Установите докер:
<br>`$ sudo apt-get install docker-ce docker-ce-cli containerd.io`
2. Добавьте своего пользователя ($USER) в группу docker:
<br>`$ sudo usermod -aG docker $USER`
1. Скачайте docker-compose и установите в папку `/usr/local/bin/docker-compose`:
<br>`$ sudo curl -L --fail https://github.com/docker/compose/releases/download/1.29.2/run.sh -o /usr/local/bin/docker-compose`
1. Установите права для управления папкой:
<br>`sudo chmod +x /usr/local/bin/docker-compose`
1. Установите гит:
<br>`$ sudo apt-get install git`
1. Зарегистрируйтесь или залогиньтесь в гите при помощи:
####
    $ git config --global user.name "<your name>"
    $ git config --global user.email "<your email>"
7. Сгенерируйте публичный ключ ssh:
<br>https://gitlab.er.ru/help/ssh/index#generate-an-ssh-key-pair
1. Подложите его на гитлабе в раздел User Settings - SSH Keys;
1. Склонируйте себе нужные репозитории из гита (например, сервер и клиент):
* Перейдите в папку, куда будете клонировать, например: `$ cd /home/<user>`
* Выполните команду для клонирования:`$ git clone <адрес репозитория>`
<br>Адрес репозитория для клонирования можно найти на гитлабе, там сверху есть синяя кнопка, нажав на которую, вы увидите несколько ссылок. Выберите ту, где Clone with SSH;
10. Перезагрузите компьютер:
<br>`$ sudo reboot`
1. Откройте файл `/etc/hosts` при помощи своего любимого текстового редактора, например:
<br>`$ sudo vi /etc/hosts`
1. Отредактируйте его, добавив следующую строку с локальными хостами проектов:
<br>`127.0.0.1       <name>.dev.local <name>.dev.local`
1. Перейдите в папку с проектом сервера, например:
<br>`$ cd /home/<user>/<project_name>`
1. Откройте файл роли docker-compose.yml при помощи своего любимого текстового редактора, например:
<br>`$ vi docker-compose.yml`
1. Добавьте все, что нужно, чтобы он выглядел так:
<details>
<summary>docker-compose.yml</summary>

```
version: '3.6'
services:
  php:
    image: registry.er.ru:443/ccet/foundation/phpmaster80:latest
    user: phpmaster
    environment:
      - SYMFONY_PHPUNIT_VERSION=9
      - SYMFONY_PHPUNIT_DIR=/app/vendor/phpunit
      - PHP_IDE_CONFIG=${PHP_IDE_CONFIG:-serverName=<name>.dev.local}
    volumes:
      - .:/app/
      - $HOME/.cache/composer:/.composer/
    extra_hosts:
      - "host.docker.internal:$DOCKER_HOST_IP"
    networks:
      - default
      - <name>-ssh

  nginx:
    image: nginx:stable
    networks:
      - outside
      - default
    environment:
      - VIRTUAL_HOST=$DOCKER_NGINX_VIRTUAL_HOST
    volumes:
      - ./nginx.conf:/etc/nginx/conf.d/default.conf
      - .:/app/

  db:
    image: <name>
    command: ["postgres", "-c", "log_statement=all"]
    volumes:
      - db_data:/var/lib/postgresql/data
    ports:
      - $DOCKER_DB_HOST_PORT:5432
    environment:
      - POSTGRES_PASSWORD=$DOCKER_POSTGRES_PASSWORD
    networks:
      - default
      - <name>

  fundsdb:
    image: <name>
    command: ["postgres", "-c", "log_statement=all"]
    volumes:
      - db_funds_data:/var/lib/postgresql/data
    ports:
      - $DOCKER_FUNDS_DB_HOST_PORT:5432
    environment:
      - POSTGRES_PASSWORD=$DOCKER_POSTGRES_PASSWORD

  redis:
    image: <name>
    ports:
      - 6379:6379

volumes:
  db_data:
  db_funds_data:

networks:
  <name>:
    name: <name>
    external: true
  <name>-ssh:
    name: <name>
    external: true
  outside:
    external: true
    name: $DOCKER_OUTSIDE_NETWORK

```
</details>
16. Настройте nginx-proxy, чтобы запускать клиент в докере:

* `$ docker network create nginx-proxy`
* Сгенерируйте себе NPM-токен:
    * Откройте гитлаб, там в настройках пользователя выберите Access Tokens;
    * В поле Token name введите npm;
    * Expiration date оставьте пустым;
    * Поставьте галку напротив read_api;
    * Нажмите Create personal access token;
    * Свежесгенерированный токен находится вверху страницы;
* Откройте текстовым редактором файл `~/.bashrc`, например:
<br>`$ sudo vi ~/.bashrc`
<br> и добавьте в него следующую строку:
<br>`export NPM_TOKEN=<токен из гитлаба>`
* Создайте файл env с токеном при помощи команды:
<br>`source ~/.bashrc`
* Создайте в директории nginx-proxy файл docker-compose.yml, например:
<br>`touch /home/<user>/projects/nginx-proxy/docker-compose.yml`
<br>со следующим содержимым:

<details>
<summary>docker-compose.yml(nginx)</summary>

```
version: '3.7'
services: 
  nginx-proxy:
    container_name: nginx-proxy
    image: jwilder/nginx-proxy
    ports:
      - 80:80
      - 443:443
    volumes:
      - /var/run/docker.sock:/tmp/docker.sock:ro
      - ./certs:/etc/nginx/certs
    networks:
      - nginx-proxy
    restart: always

networks:
  nginx-proxy:
    external: true

```
</details>

* Создайте директорию для сертификатов в директории, где лежат проекты, например:
<br>`$ mkdir /home/<user>/nginx-proxy/certs`
* Скачайте сертификаты с официального сайта, например:
<br>`$ wget https://github.com/FiloSottile/mkcert/releases/download/v1.4.3/mkcert-v1.4.3-linux-amd64`
* Переместите сертификаты в папку `/usr/local/bin/` с названием mkcert:
<br>`$ sudo mv mkcert-v1.4.3-linux-amd64 /usr/local/bin/mkcert`
* Дайте права:
<br>`sudo chmod +x /usr/local/bin/mkcert`
* Установите libnss3-tools, чтобы установить эти сертификаты:
<br>`$ sudo apt install libnss3-tools`
* Перейдите в свежесозданную директорию с сертификатами:
<br>`$ cd /home/<user>/nginx-proxy/certs`
* Установите сертификаты:
   * `mkcert -install`
   * `mkcert *.dev.local`
* Переименуйте их, чтобы названия совпадали с принятыми в nginx:
####
    $ mv _wildcard.dev.local.pem dev.local.crt
    mv _wildcard.dev.local-key.pem dev.local.key
17. Перейдите в папку с серверным проектом:
<br>`$ cd /home/<user>/<name>`
1. Создайте сети для запуска проектов:
####
    $ docker network create <name>
    docker network create <name>-ssh
19. Перезапустите докер:
  <br>  `$ dco restart`
20. Зайдите в докер и там установите менеджер зависимостей php:
<br>`$ dco exec php composer install`
1. Выполните миграции в докере:
<br>`$ dco exec php console d:m:m -n`
22. Загрузите фикстуры (тестовые данные):
####
    $ dco exec php php -d memory_limit=512M bin/console doctrine:fixtures:load -n --group=main --em=default --no-debug
    dco exec php php -d memory_limit=512M bin/console doctrine:fixtures:load -n --group=funds --em=funds --no-debug
23. Перейдите в проект клиента, например:
<br>`$ cd /home/<user>/<name>`
1. Переименуйте docker-compose.template.yml в docker-compose.yml:
<br>`$ mv docker-compose.template.yml в docker-compose.yml`
1. Запустите в докере установку yarn с удалением контейнера:
<br>`$ dco run --rm app yarn`
1. Перезапустите докер:
<br>`$ dco restart`
1. Откройте в браузере страницу:
<br>[https://\<name\>.dev.local/](https://\<name\>.dev.local/)
1. Перейдите в директорию с проектом, из которого нужно развернуть ветку
1. Чтобы увидеть доступные ветки, выполните команду:
<br>`$ git branch -a`
1. Перейдите на ветку, которую нужно развернуть локально:
<br>`$ git checkout <название ветки>`
1. Запустите сборку проекта (из директории с сервером и из директории с клиентом) в докере:
<br>`$ dco up -d`

В браузере должен отобразиться прогресс-бар сборки клиента и по итогу должна отобразиться нужная ветка.
